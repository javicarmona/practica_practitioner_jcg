const requestJson = require('request-json');
const jwt = require('jsonwebtoken');

const mLabURL = "https://api.mlab.com/api/1/databases/apitechujcg6ed/collections/";
const mLabAPIKey = "apiKey=E61WZ1PMqQky2UAZcKzNynMKzqK-TmN-";

function esTokenErroneo(usuario,token){
  //Decode the token
  jwt.verify(token,"samplesecret",(err,decod)=>{
    return err;
    });
}

function createOperacionCajeroV1(req,res){
  console.log("**********************************************");
  console.log("POST /APIProyectoJCG/v1/operacionCajero/:token");
  console.log("**********************************************");
  var usuarioOperacion = req.body.cod_usuario;
  var tokenOperacion = req.params.token;

  if (esTokenErroneo(usuarioOperacion,tokenOperacion)){
    //console.log("Token usuario erroneo");
    var response = {
      "msg" : "Token usuario erroneo"
    }
    //res.status(404);
    res.send(response);
  }else{
    //console.log("Token usuario validado");

  var cuentaOperacion = req.body.IBAN;
  var importeOperacion = req.body.importe_mov;
  var tipoOperacion = req.body.tipo_operacion;
  //console.log("cod_usuario es " + usuarioOperacion);
  //console.log("IBAN es " + cuentaOperacion);
  //console.log("tipoOperacion es " + tipoOperacion);
  //console.log("fecha_mov es " + req.body.fecha_mov);
  //console.log("concepto_mov es " + req.body.concepto_mov);
  //console.log("importe_mov es " + importeOperacion);
  //console.log("divisa_mov es " + req.body.divisa_mov);

  var nuevaOperacionCajero = {
    "cod_usuario" : usuarioOperacion,
    "IBAN" : cuentaOperacion,
    "tipo_operacion" : tipoOperacion,
    "fecha_mov" : req.body.fecha_mov,
    "concepto_mov" : req.body.concepto_mov,
    "importe_mov" : importeOperacion,
    "divisa_mov" : req.body.divisa_mov
  };

  var queryCuentaUsuario = 'q={';
  queryCuentaUsuario += '"cod_usuario" : "' + usuarioOperacion + '",';
  queryCuentaUsuario += '"IBAN" : "' + cuentaOperacion + '"';
  queryCuentaUsuario += '}';
  //console.log("query es "+queryCuentaUsuario);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("Conectado con MLab");

  mLabCliente.get("appJCGCuentas?" + queryCuentaUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          //console.log("Error en consulta de cuenta de usuario");
          response = {
              "msg" : "Error en consulta de cuenta de usuario"
            }
            res.status(500);
            res.send(response);
        }else{
          if (body.length > 0){
            //console.log("Cuenta de Usuario encontrado");
            var newImporteCuentaUsuario = parseInt(body[0].saldo_Cuenta);
            if (tipoOperacion == "Ingreso"){
              newImporteCuentaUsuario += parseInt(importeOperacion);
            }
            if (tipoOperacion == "Retirada"){
              newImporteCuentaUsuario -= parseInt(importeOperacion);
            }
            //console.log("Nuevo importe de cuenta: "+newImporteCuentaUsuario);
            var putBody = '{"$set":{"saldo_Cuenta":"'+newImporteCuentaUsuario+'"}}';
            //console.log("PutBody is " + putBody);
            mLabCliente.put("appJCGCuentas?" + queryCuentaUsuario + "&" + mLabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT){
                //console.log("Importe de Cuenta de Usuario actualizado con exito");
                //res.send({"msg" : "Importe de Cuenta de Usuario actualizado con exito"})
              }
            )
            mLabCliente.post("appJCGMovimientos?" + mLabAPIKey, nuevaOperacionCajero,
              function(err, resMLab, body){
                //console.log("Movimiento de Cuenta de Usuario creado con exito");
                //res.send({"msg" : "Movimiento de Cuenta de Usuario creado con exito"})
              }
            )
            var response = {
              "msg" : "Movimiento creado"
            }
            res.send(response);
          }else{
            //console.log("Cuenta de Usuario no encontrado -> NO Dar de alta");
            var response = {
              "msg" : "Cuenta de Usuario no encontrado -> NO Dar de alta"
            }
            res.status(404);
            res.send(response);
          }
        }
      }
  );
}
}

function createEnvioDineroV1(req,res){
  console.log("******************************************");
  console.log("POST /APIProyectoJCG/v1/envioDinero/:token");
  console.log("******************************************");
  var usuarioOperacion = req.body.cod_usuario;
  var tokenOperacion = req.params.token;

  if (esTokenErroneo(usuarioOperacion,tokenOperacion)){
    //console.log("Token usuario erroneo");
    var response = {
      "msg" : "Token usuario erroneo"
    }
    //res.status(404);
    res.send(response);
  }else{
    //console.log("Token usuario validado");

  var cuentaOperacion = "";
  var cuentaOrigenOperacion = req.body.IBAN_origen;
  var cuentaDestinoOperacion = req.body.IBAN_destino;
  var tipoOperacion = "";
  var importeOperacion = req.body.importe_mov;
  var f = new Date();
  var fechaOperacion = (f.getDate()) + "/" + (f.getMonth() +1) + "/" + (f.getFullYear());
  //console.log("cod_usuario es " + usuarioOperacion);
  //console.log("IBAN origen es " + cuentaOrigenOperacion);
  //console.log("IBAN destino es " + cuentaDestinoOperacion);
  //console.log("fecha_mov es " + fechaOperacion);
  //console.log("concepto_mov es " + req.body.concepto_mov);
  //console.log("importe_mov es " + importeOperacion);
  //console.log("divisa_mov es " + req.body.divisa_mov);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("Conectado con MLab");

  var nuevaOperacionEnvio = {
    "cod_usuario" : usuarioOperacion,
    "IBAN" : cuentaOperacion,
    "tipo_operacion" : tipoOperacion,
    "fecha_mov" : fechaOperacion,
    "concepto_mov" : req.body.concepto_mov,
    "importe_mov" : importeOperacion,
    "divisa_mov" : req.body.divisa_mov
  };

  var queryCuentaDestinoUsuario = 'q={';
  queryCuentaDestinoUsuario += '"cod_usuario" : "' + usuarioOperacion + '",';
  queryCuentaDestinoUsuario += '"IBAN" : "' + cuentaDestinoOperacion + '"';
  queryCuentaDestinoUsuario += '}';
  //console.log("query Cuenta Destino es "+queryCuentaDestinoUsuario);

  mLabCliente.get("appJCGCuentas?" + queryCuentaDestinoUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (body.length > 0){
        //console.log("Cuenta de Destino encontrada");
        nuevaOperacionEnvio.IBAN = cuentaDestinoOperacion;
        nuevaOperacionEnvio.tipo_operacion = "TRASPASO";
        var newImporteCuentaDestinoUsuario = parseInt(body[0].saldo_Cuenta);
        newImporteCuentaDestinoUsuario += parseInt(importeOperacion);
        //console.log("Nuevo importe de cuenta: "+newImporteCuentaDestinoUsuario);
        var putBody = '{"$set":{"saldo_Cuenta":"'+newImporteCuentaDestinoUsuario+'"}}';
        //console.log("PutBody is " + putBody);
        mLabCliente.put("appJCGCuentas?" + queryCuentaDestinoUsuario + "&" + mLabAPIKey, JSON.parse(putBody),
          function(errPUT, resMLabPUT, bodyPUT){
            //console.log("Importe de Cuenta Destino de Usuario actualizado con exito");
          }
        )
        mLabCliente.post("appJCGMovimientos?" + mLabAPIKey, nuevaOperacionEnvio,
          function(err, resMLab, body){
            //console.log("Movimiento de Cuenta Destino de Usuario creado con exito");
          }
        )
      }else{
        nuevaOperacionEnvio.tipo_operacion = "TRANSFERENCIA";
      }
    }
  )

  var queryCuentaUsuario = 'q={';
  queryCuentaUsuario += '"cod_usuario" : "' + usuarioOperacion + '",';
  queryCuentaUsuario += '"IBAN" : "' + cuentaOrigenOperacion + '"';
  queryCuentaUsuario += '}';
  //console.log("query Cuenta Origen es "+queryCuentaUsuario);

  mLabCliente.get("appJCGCuentas?" + queryCuentaUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          //console.log("Error en consulta de cuenta de usuario");
          response = {
              "msg" : "Error en consulta de cuenta de usuario"
            }
            res.status(500);
            res.send(response);
        }else{
          if (body.length > 0){
            //console.log("Cuenta de Origen encontrada");
            nuevaOperacionEnvio.IBAN = cuentaOrigenOperacion;
            var newImporteCuentaUsuario = parseInt(body[0].saldo_Cuenta);
            newImporteCuentaUsuario -= parseInt(importeOperacion);
            //console.log("Nuevo importe de cuenta: "+newImporteCuentaUsuario);
            var putBody = '{"$set":{"saldo_Cuenta":"'+newImporteCuentaUsuario+'"}}';
            //console.log("PutBody is " + putBody);
            mLabCliente.put("appJCGCuentas?" + queryCuentaUsuario + "&" + mLabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT){
                //console.log("Importe de Cuenta Origen de Usuario actualizado con exito");
                //res.send({"msg" : "Importe de Cuenta de Usuario actualizado con exito"})
              }
            )
            mLabCliente.post("appJCGMovimientos?" + mLabAPIKey, nuevaOperacionEnvio,
              function(err, resMLab, body){
                //console.log("Movimiento de Cuenta Origen de Usuario creado con exito");
                //res.send({"msg" : "Movimiento de Cuenta de Usuario creado con exito"})
              }
            )
            var response = {
              "msg" : "Operación finaliza correctamente"
            }
            res.send(response);
          }else{
            //console.log("Cuenta de Usuario no encontrado -> NO Dar de alta");
            var response = {
              "msg" : "Cuenta de Usuario no encontrado -> NO Dar de alta"
            }
            res.status(404);
            res.send(response);
          }
        }
      }
  );
}
}

function getMovimientosCuentaUsuarioV1(req,res){
  console.log("********************************************************************");
  console.log("GET /APIProyectoJCG/v1/appJCGMovimientos/:idUsuario/:idCuenta/:token");
  console.log("********************************************************************");
  var usuarioConsultar = req.params.idUsuario;
  var tokenOperacion = req.params.token;

  if (esTokenErroneo(usuarioConsultar,tokenOperacion)){
    //console.log("Token usuario erroneo");
    var response = {
      "msg" : "Token usuario erroneo"
    }
    //res.status(404);
    res.send(response);
  }else{
    //console.log("Token usuario validado");
  var cuentaConsultar = req.params.idCuenta;
  //console.log("usuario es " + usuarioConsultar);
  //console.log("cuenta es " + cuentaConsultar);

  var queryCuentaUsuario = 'q={';
  queryCuentaUsuario += '"cod_usuario" : "' + usuarioConsultar + '",';
  queryCuentaUsuario += '"IBAN" : "' + cuentaConsultar + '"';
  queryCuentaUsuario += '}';
  //console.log("query es "+queryCuentaUsuario);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("Conectado con MLab");

  mLabCliente.get("appJCGMovimientos?" + queryCuentaUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          //console.log("Error en la consulta de movimientos de la cuenta del usuario");
          response = {
              "msg" : "Error en la consulta de movimientos de la cuenta del usuario"
            }
            res.status(500);
            //res.send(response);
        }else{
          if (body.length > 0){
            //console.log("El usuario tiene movimientos");
            var response = body;
            //res.send(response);
          }else{
              //console.log("El usuario NO tiene movimientos");
              var response = {
              "msg" : "El usuario NO tiene movimientos"
              }
              res.status(404);
              //res.send(response);
          }
        }
        res.send(response);
      }
    );
  }
}
module.exports.createOperacionCajeroV1 = createOperacionCajeroV1;
module.exports.createEnvioDineroV1 = createEnvioDineroV1;
module.exports.getMovimientosCuentaUsuarioV1 = getMovimientosCuentaUsuarioV1;
