const requestJson = require('request-json');
const jwt = require('jsonwebtoken');

const mLabURL = "https://api.mlab.com/api/1/databases/apitechujcg6ed/collections/";
const mLabAPIKey = "apiKey=E61WZ1PMqQky2UAZcKzNynMKzqK-TmN-";

function esTokenErroneo(usuario,token){
  //Decode the token
  jwt.verify(token,"samplesecret",(err,decod)=>{
    return err;
    });
}

function createCuentaUsuarioV1(req,res){
  console.log("*******************************************************");
  console.log("POST /APIProyectoJCG/v1/appJCGCuentas/:idUsuario/:token");
  console.log("*******************************************************");
  var usuarioCrear = req.params.idUsuario;
  var tokenUsuarioCrear = req.params.token;
  //console.log("cod_usuario es " + usuarioCrear);
  //console.log("token es " + tokenUsuarioCrear);
  //console.log("banco_Cuenta es " + req.body.banco_Cuenta);
  //console.log("tipo_Cuenta es " + req.body.tipo_Cuenta);
  //console.log("IBAN es " + req.body.IBAN);
  //console.log("saldo_Cuenta es " + req.body.saldo_Cuenta);
  //console.log("divisa_Cuenta es " + req.body.divisa_Cuenta);

  if (esTokenErroneo(usuarioCrear,tokenUsuarioCrear)){
    //console.log("Token usuario erroneo");
    var response = {
      "msg" : "Token usuario erroneo"
    }
    //res.status(404);
    res.send(response);
  }else{
    //console.log("Token usuario validado");

  var nuevaCuentaUsuario = {
    "cod_usuario" : usuarioCrear,
    "banco_Cuenta" : req.body.banco_Cuenta,
    "tipo_Cuenta" : req.body.tipo_Cuenta,
    "IBAN" : req.body.IBAN,
    "saldo_Cuenta" : req.body.saldo_Cuenta,
    "divisa_Cuenta" : req.body.divisa_Cuenta
  };

  var queryUsuario = 'q={"cod_usuario" : "' + usuarioCrear + '"}';
  //console.log("query es "+queryUsuario);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("Conectado con MLab");

  mLabCliente.get("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          //console.log("Error en consulta de usuario");
          response = {
              "msg" : "Error en consulta de usuario"
            }
            res.status(500);
            res.send(response);
        }else{
          if (body.length > 0){
            //console.log("Usuario encontrado");
            mLabCliente.post("appJCGCuentas?" + mLabAPIKey, nuevaCuentaUsuario,
              function(err, resMLab, body){
                //console.log("Cuenta de Usuario creado con exito");
                res.send({"msg" : "Cuenta de Usuario creado con exito"})
              }
            )
          }else{
            //console.log("Usuario no encontrado -> NO Dar de alta");
            var response = {
              "msg" : "Usuario no encontrado -> NO Dar de alta"
            }
            res.status(404);
            res.send(response);
          }
        }
      }
  );
}
}

function getCuentasUsuarioV1(req,res){
  console.log("******************************************************");
  console.log("GET /APIProyectoJCG/v1/appJCGCuentas/:idUsuario/:token");
  console.log("******************************************************");
  //var usuarioConsultar = req.params.idUsuario;
  //console.log("cod_usuario es " + usuarioConsultar);
  var usuarioConsultar = req.params.idUsuario;
  var tokenUsuarioConsultar = req.params.token;
  //console.log("cod_usuario es " + usuarioConsultar);
  //console.log("token es " + tokenUsuarioConsultar);

  if (esTokenErroneo(usuarioConsultar,tokenUsuarioConsultar)){
    //console.log("Token usuario erroneo");
    var response = {
      "msg" : "Token usuario erroneo"
    }
    //res.status(404);
    res.send(response);
  }else{
    //console.log("Token usuario validado");

  var queryUsuario = 'q={"cod_usuario" : "' + usuarioConsultar + '"}';
  //console.log("appJCGCuentas::query es "+queryUsuario);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("appJCGCuentas::Conectado con MLab");

  mLabCliente.get("appJCGCuentas?" + queryUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          //console.log("Error en la consulta de cuentas del usuario");
          response = {
              "msg" : "Error en la consulta de cuentas del usuario"
            }
            res.status(500);
            //res.send(response);
        }else{
          if (body.length > 0){
            //console.log("appJCGCuentas::El usuario tiene cuentas");
            var response = body;
            //res.send(response);
          }else{
              //console.log("El usuario NO tiene cuentas");
              var response = {
              "msg" : "El usuario NO tiene cuentas"
              }
              res.status(404);
              //res.send(response);
          }
        }
        res.send(response);
      }
    );
  }
}

  function deleteCuentaUsuarioV1(req,res){
    console.log("****************************************************************");
    console.log("PUT /APIProyectoJCG/v1/appJCGCuentas/:idUsuario/:idCuenta/:token");
    console.log("****************************************************************");

    var usuarioBorrar = req.params.idUsuario;
    var cuentaBorrar = req.params.idCuenta;
    var tokenUsuarioBorrar = req.params.token;
    //console.log("cod_usuario es " + usuarioBorrar);
    //console.log("token es " + tokenUsuarioBorrar);
    //console.log("IBAN es " + cuentaBorrar);

    if (esTokenErroneo(usuarioBorrar,tokenUsuarioBorrar)){
      //console.log("Token usuario erroneo");
      var response = {
        "msg" : "Token usuario erroneo"
      }
      //res.status(404);
      res.send(response);
    }else{
      //console.log("Token usuario validado");
    var queryCuentaUsuario = 'q={';
    queryCuentaUsuario += '"cod_usuario" : "' + usuarioBorrar + '",';
    queryCuentaUsuario += '"IBAN" : "' + cuentaBorrar + '"';
    queryCuentaUsuario += '}';
    //console.log("query es "+queryCuentaUsuario);

    var mLabCliente = requestJson.createClient(mLabURL);
    //console.log("Conectado con MLab");

    mLabCliente.get("appJCGCuentas?" + queryCuentaUsuario + "&" + mLabAPIKey,
      function(err, resMLab, body){
        if (err){
          //console.log("Error en consulta de cuenta de usuario");
          response = {
              "msg" : "Error en consulta de cuenta de usuario"
          }
          res.status(500);
          res.send(response);
        }else{
          if (body.length > 0){
            //console.log("Cuenta de Usuario encontrado");
            var putBody = '{}';
            mLabCliente.put("appJCGCuentas?" + queryCuentaUsuario + "&" + mLabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT){
                if (err){
                  //console.log("Error en eliminación de cuenta de usuario");
                  response = {
                      "msg" : "Error en eliminación de cuenta de usuario"
                  }
                  res.status(500);
                  res.send(response);
                }else{
                  //console.log("Baja realizada correctamente");
                  res.send({"msg" : "Baja realizada correctamente"})
                }
              }
            )
          }
        }
      }
    )
  }
}
  module.exports.createCuentaUsuarioV1 = createCuentaUsuarioV1;
  module.exports.getCuentasUsuarioV1 = getCuentasUsuarioV1;
  module.exports.deleteCuentaUsuarioV1 = deleteCuentaUsuarioV1;
