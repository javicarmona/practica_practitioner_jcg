const crypt = require('../crypt');
const jwt = require('jsonwebtoken');
const requestJson = require('request-json');
const mLabURL = "https://api.mlab.com/api/1/databases/apitechujcg6ed/collections/";
const mLabAPIKey = "apiKey=E61WZ1PMqQky2UAZcKzNynMKzqK-TmN-";

function loginUsuarioV1(req,res){
  console.log("******************************");
  console.log("POST /APIProyectoJCG/v1/login/");
  console.log("******************************");
  var usuarioLoginUsuario = req.body.cod_usuario;
  var usuarioLoginPwd = req.body.clave_acceso;

  var queryUsuario = 'q={"cod_usuario" : "' + usuarioLoginUsuario + '"}';
  //console.log("query es "+queryUsuario);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("Conectado con MLab");

  mLabCliente.get("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          //console.log("Error realizando login");
          response = {
              "msg" : "Error realizando login"
            }
            res.status(500);
            res.send(response);

        }else{
          if (body.length > 0){
            //console.log("Usuario encontrado");
            var claveAccesoUsuario = body[0].clave_acceso;
            var isPwdCorrect = crypt.checkPassword(usuarioLoginPwd,claveAccesoUsuario);
            //var isPwdCorrect = (usuarioLoginPwd == usuarioMongoPwd);
            if (isPwdCorrect){
              //console.log("Usuario y password coinciden");
              //console.log("LOGIN correcto");
              //crear el token.
              var token=jwt.sign(usuarioLoginUsuario,"samplesecret");
              //enviar respuesta
              var response = {
                "msg" : "LOGIN correcto",
                "error" : "0",
                "cod_usuario" : body[0].cod_usuario,
                "token" : token
              }
              res.send(response);
              //query = 'q={"id" : ' + body[0].id +'}';
              //console.log("Query for put is " + query);
              /*var putBody = '{"$set":{"logged":true}}';
              mLabCliente.put("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT){
                  console.log("LOGIN correcto");
                  //crear el token.
                  //var token=jwt.sign(user,"samplesecret");
                  //enviar respuesta
                  var response = {
                    "msg" : "LOGIN correcto",
                    "error" : "0",
                    "cod_usuario" : body[0].cod_usuario
                    //"token" : token
                  }
                  res.send(response);
                }
              );*/
            }else{
              //console.log("Clave de acceso incorrecta");
              var response = {
                "msg" : "Clave de acceso incorrecta",
                "error" : "401"
              }
              //res.status(500);
              res.send(response);
            }
          }else{
              //console.log("Usuario no registrado");
              var response = {
                "msg" : "Usuario no registrado",
                "error" : "400"
              }
              //res.status(500);
              res.send(response);
          }
        }
      }
  );
}

function logoutUsuarioV1(req,res){
  console.log("******************************");
  console.log("POST /APIProyectoJCG/v1/logout");
  console.log("******************************");
  //var usuarioLogout = req.params.cod_usuario;
  var usuarioLogout = req.body.cod_usuario;
  //console.log("cod_usuario logout : "+usuarioLogout);

  var queryUsuario = 'q={"cod_usuario" : "' + usuarioLogout + '"}';
  //console.log("query es "+queryUsuario);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("Conectado con MLab");

  mLabCliente.get("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        //console.log("Error realizando logout");
        response = {
            "msg" : "Error realizando logout"
        }
        res.status(500);
        res.send(response);

      }else{
        if (body.length > 0){
          //console.log("Usuario encontrado");
          //console.log("LOGOUT correcto");
          response = {
            "msg" : "LOGOUT correcto",
            "idUsuario" : body[0].cod_usuario
          }
          res.send(response);
          /*var putBody = '{"$unset":{"logged":""}}';
          mLabCliente.put("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT){
              console.log("LOGOUT correcto");
              response = {
                "msg" : "LOGOUT correcto",
                "idUsuario" : body[0].cod_usuario
              }
              res.send(response);
            }
          );*/
        }else{
            //console.log("LOGOUT incorrecto");
            response = {
            "msg" : "LOGOUT incorrecto"
            }
          res.status(404);
          res.send(response);

        }
      }
    }
  );
}

module.exports.loginUsuarioV1 = loginUsuarioV1;
module.exports.logoutUsuarioV1 = logoutUsuarioV1;
