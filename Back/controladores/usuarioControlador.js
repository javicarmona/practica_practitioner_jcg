const requestJson = require('request-json');
const jwt = require('jsonwebtoken');
const crypt = require('../crypt');

const mLabURL = "https://api.mlab.com/api/1/databases/apitechujcg6ed/collections/";
const mLabAPIKey = "apiKey=E61WZ1PMqQky2UAZcKzNynMKzqK-TmN-";

function esTokenErroneo(usuario,token){
  //Decode the token
  jwt.verify(token,"samplesecret",(err,decod)=>{
    return err;
    });
}

function createUsuarioV1(req,res){
  console.log("**************************************");
  console.log("POST /APIProyectoJCG/v1/appJCGUsuarios");
  console.log("**************************************");
  var usuarioCrear = req.body.cod_usuario;
  //console.log("cod_usuario es " + usuarioCrear);
  //console.log("nombre es " + req.body.nombre);
  //console.log("apellido1 es " + req.body.apellido1);
  //console.log("apellido2 es " + req.body.apellido2);
  //console.log("movil es " + req.body.movil);
  //console.log("email es " + req.body.email);
  //console.log("direccion es " + req.body.direccion);
  //console.log("clave_acceso es " + req.body.clave_acceso);
  //console.log("clave_operaciones es " + req.body.clave_operaciones);
  //console.log("pregunta_clave_acceso es " + req.body.pregunta_clave_acceso);
  //console.log("respuesta_clave_acceso es " + req.body.respuesta_clave_acceso);

  var nuevoUsuario = {
    "cod_usuario" : req.body.cod_usuario,
    "nombre" : req.body.nombre,
    "apellido1" : req.body.apellido1,
    "apellido2" : req.body.apellido2,
    "movil" : req.body.movil,
    "email" : req.body.email,
    "direccion" : req.body.direccion,
    "clave_acceso" : crypt.hash(req.body.clave_acceso),
    //"clave_acceso" : req.body.clave_acceso,
    "clave_operaciones" : crypt.hash(req.body.clave_operaciones),
    //"clave_operaciones" : req.body.clave_operaciones,
    "pregunta_clave_acceso" : req.body.pregunta_clave_acceso,
    "respuesta_clave_acceso" : req.body.respuesta_clave_acceso
  };

  var queryUsuario = 'q={"cod_usuario" : "' + usuarioCrear + '"}';
  //console.log("query es "+queryUsuario);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("Conectado con MLab");

  mLabCliente.get("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          //console.log("Error en alta de usuario");
          response = {
              "msg" : "Error en alta de usuario"
            }
            res.status(500);
            res.send(response);
        }else{
          if (body.length > 0){
            //console.log("Usuario ya existe");
            var response = {
              "msg" : "Usuario ya existe",
              "error" : "400"
            }
            //res.status(404);
            res.send(response);
          }else{
            //console.log("Usuario no encontrado -> Dar de alta");
            mLabCliente.post("appJCGUsuarios?" + mLabAPIKey, nuevoUsuario,
              function(err, resMLab, body){
                //console.log("Usuario creado con exito");
                var response = {
                  "msg" : "Usuario creado con exito",
                  "error" : "0"
                }
                //res.status(404);
                res.send(response);
              }
            )
          }
        }
      }
  );
}

function reactivaClaveUsuarioV1(req,res){
  console.log("**********************************************************");
  console.log("PUT /APIProyectoJCG/v1/appJCGUsuarios/reactivaClaveUsuario");
  console.log("**********************************************************");
  var usuarioModificar = req.body.cod_usuario;
  var usuarioPwdNew = crypt.hash(req.body.clave_acceso_nueva);
  var preguntaPwd = req.body.pregunta_clave_acceso;
  var respuestaPwd = req.body.respuesta_clave_acceso;

  //console.log("cod_usuario es " + usuarioModificar);
  //console.log("clave_acceso_nueva es " + usuarioPwdNew);
  //console.log("pregunta_clave_acceso es " + preguntaPwd);
  //console.log("respuesta_clave_acceso es " + respuestaPwd);

  var queryUsuario = 'q={"cod_usuario" : "' + usuarioModificar + '"}';
  //console.log("query es "+queryUsuario);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("Conectado con MLab");


  mLabCliente.get("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          //console.log("Error al buscar el usuario");
          response = {
              "msg" : "Error al buscar el usuario"
            }
            res.status(500);
            res.send(response);
        }else{
          if (body.length > 0){
            //console.log("Usuario encontrado");
            var preguntaSeguridadUsuario = body[0].pregunta_clave_acceso;
            var respuestaSeguridadUsuario = body[0].respuesta_clave_acceso;
            var cumpleSeguridad = (preguntaSeguridadUsuario == preguntaPwd) && (respuestaSeguridadUsuario == respuestaPwd);
            if (cumpleSeguridad){
              //console.log("Cumple la pregunta y respuesta de Seguridad");
              //query = 'q={"cod_usuario" : "' + body[0].cod_usuario +'"}';
              //console.log("Query for put is " + query);
              //var putBody = '{"$set":{"clave_acceso":'+crypt.hash(usuarioLoginPwdNew)+'}}';
              var putBody = '{"$set":{"clave_acceso":"'+usuarioPwdNew+'"}}';
              //console.log("PutBody is " + putBody);
              mLabCliente.put("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT){
                  /*if (err){
                    console.log("Error al actualizar la clave de acceso");
                    var response = {
                        "msg" : "Error al actualizar la clave de acceso"
                    }
                    res.status(500);
                    res.send(response);
                  }else{*/
                    //console.log("Su clave de acceso se ha actualizado correctamente");
                    var response = {
                      "msg" : "Su clave de acceso se ha actualizado correctamente",
                      "error" : "0"
                    }
                    res.send(response);
                  //}
                }
              );
            }else{
              //console.log("No cumple la seguridad");
              var response = {
                "msg" : "No cumple la seguridad",
                "error" : "401"
              }
              //res.status(404);
              res.send(response);
            }
          }else{
              //console.log("Usuario no registrado");
              var response = {
                "msg" : "Usuario no registrado",
                "error" : "400"
              }
              //res.status(404);
              res.send(response);
          }
        }
      }
  );
}

function deleteUsuarioV1(req,res){
  console.log("*************************************************");
  console.log("DELETE /APIProyectoJCG/v1/appJCGUsuarios/bajaUsuario");
  console.log("*************************************************");
  var usuarioEliminar = req.body.cod_usuario;
  var usuarioPwd = req.body.clave_acceso;

  //console.log("cod_usuario es " + usuarioEliminar);
  //console.log("clave_acceso es " + usuarioPwd);

  var queryUsuario = 'q={"cod_usuario" : "' + usuarioEliminar + '"}';
  //console.log("query es "+queryUsuario);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("Conectado con MLab");

  mLabCliente.get("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          //console.log("Error al buscar el usuario");
          response = {
              "msg" : "Error al buscar el usuario"
            }
            res.status(500);
            res.send(response);
        }else{
          if (body.length > 0){
            //console.log("Usuario encontrado");
            var claveAccesoUsuario = body[0].clave_acceso;
            var isPwdCorrect = crypt.checkPassword(usuarioPwd,claveAccesoUsuario);
            //var isPwdCorrect = (usuarioPwd == claveAccesoUsuario);
            if (isPwdCorrect){
              //console.log("Usuario y password coinciden");
              var putBody = '{}';
              //console.log("PutBody is " + putBody);
              mLabCliente.put("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT){
                  if (err){
                    //console.log("Error en DELETE");
                    var response = {
                        "msg" : "Error en DELETE"
                    }
                    res.status(500);
                    res.send(response);
                  }else{
                    //console.log("DELETE correcto");
                    var response = {
                      "msg" : "DELETE correcto",
                      "error" : "0"
                    }
                    res.send(response);
                  }
                }
              );
              /** ¿¿FALTA  EL BORRADO DE CUENTAS Y MOVIMIENTOS?? **/
            }else{
              //console.log("Clave de acceso incorrecta");
              var response = {
                "msg" : "Clave de acceso incorrecta",
                "error" : "401"
              }
              //res.status(404);
              res.send(response);
            }
          }else{
              //console.log("Usuario no registrado");
              var response = {
                "msg" : "Usuario no registrado",
                "error" : "400"
              }
              //res.status(404);
              res.send(response);
          }
        }
      }
  );
}

function getUsuarioByIdV1(req,res){
  console.log("*******************************************************");
  console.log("GET /APIProyectoJCG/v1/appJCGUsuarios/:idUsuario/:token");
  console.log("*******************************************************");
  var usuarioConsultar = req.params.idUsuario;
  var tokenUsuarioConsultar = req.params.token;
  var usuarioBodyConsultar = req.body.cod_usuario;
  var tokenUsuarioBodyConsultar = req.body.token;
  //console.log("usuario Param es " + usuarioConsultar);
  //console.log("token_usuario Param es " + tokenUsuarioConsultar);
  //console.log("usuario Body es " + usuarioBodyConsultar);
  //console.log("token_usuario Body es " + tokenUsuarioBodyConsultar);

  if (esTokenErroneo(usuarioConsultar,tokenUsuarioConsultar)){
    //console.log("Token usuario erroneo");
    var response = {
      "msg" : "Token usuario erroneo"
    }
    //res.status(404);
    res.send(response);
  }else{
    //console.log("Token usuario validado");
    var queryUsuario = 'q={"cod_usuario" : "' + usuarioConsultar + '"}';
    //console.log("appJCGUsuarios::query es "+queryUsuario);

    var mLabCliente = requestJson.createClient(mLabURL);
    //console.log("appJCGUsuarios::Conectado con MLab");

    mLabCliente.get("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey,
      function(err, resMLab, body){
          if (err){
            //console.log("Error en la consulta del usuario");
            response = {
                "msg" : "Error en la consulta del usuario"
              }
              res.status(500);
              res.send(response);
          }else{
            if (body.length > 0){
              //console.log("appJCGUsuarios::Usuario encontrado");
              var response = body[0];
              res.send(response);
            }else{
                //console.log("Usuario NO encontrado");
                var response = {
                  "msg" : "Usuario NO encontrado"
                }
                //res.status(404);
                res.send(response);
            }
          }
        }
      );
    }
  }

function updateClaveUsuarioV1(req,res){
  console.log("***************************************************************");
  console.log("PUT /APIProyectoJCG/v1/appJCGUsuarios/updateClaveUsuario/:token");
  console.log("***************************************************************");
  var usuarioOperacion = req.body.cod_usuario;
  var tokenOperacion = req.params.token;

  if (esTokenErroneo(usuarioOperacion,tokenOperacion)){
    //console.log("Token usuario erroneo");
    var response = {
      "msg" : "Token usuario erroneo"
    }
    //res.status(404);
    res.send(response);
  }else{
    //console.log("Token usuario validado");
  var usuarioPwdOld = req.body.clave_acceso_actual;
  var usuarioPwdNew = req.body.clave_acceso_nueva;

  //console.log("cod_usuario es " + usuarioOperacion);
  //console.log("clave_acceso_actual es " + usuarioPwdOld);
  //console.log("clave_acceso_nueva es " + usuarioPwdNew);

  var queryUsuario = 'q={"cod_usuario" : "' + usuarioOperacion + '"}';
  //console.log("query es "+queryUsuario);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("Conectado con MLab");


  mLabCliente.get("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          //console.log("Error al buscar el usuario");
          response = {
              "msg" : "Error al buscar el usuario"
            }
            res.status(500);
            res.send(response);
        }else{
          if (body.length > 0){
            //console.log("Usuario encontrado");
            var claveAccesoUsuario = body[0].clave_acceso;
            //var isPwdCorrect = (usuarioPwdOld == claveAccesoUsuario);
            var isPwdCorrect = crypt.checkPassword(usuarioPwdOld,claveAccesoUsuario);
            if (isPwdCorrect){
              //console.log("Usuario y password coinciden");
              var putBody = '{"$set":{"clave_acceso":"'+usuarioPwdNew+'"}}';
              //console.log("PutBody is " + putBody);
              mLabCliente.put("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT){
                  /*if (err){
                    console.log("Error al actualizar la clave de acceso");
                    var response = {
                        "msg" : "Error al actualizar la clave de acceso"
                    }
                    res.status(500);
                    res.send(response);
                  }else{*/
                    //console.log("Su clave de acceso se ha actualizado correctamente");
                    var response = {
                      "msg" : "Su clave de acceso se ha actualizado correctamente",
                      "error" : "0"
                    }
                    res.send(response);
                  //}
                }
              );
            }else{
              //console.log("Clave de acceso ACTUAL incorrecta");
              var response = {
                "msg" : "Clave de acceso ACTUAL incorrecta",
                "error" : "401"
              }
              //res.status(404);
              res.send(response);
            }
          }else{
              //console.log("Usuario no registrado");
              var response = {
                "msg" : "Usuario no registrado",
                "error" : "400"
              }
              //res.status(404);
              res.send(response);
          }
        }
      }
  );
}
}
function updateDatosContactoV1(req,res){
  console.log("**********************************************************");
  console.log("PUT /APIProyectoJCG/v1/appJCGUsuarios/datosContacto/:token");
  console.log("**********************************************************");
  var usuarioOperacion = req.body.cod_usuario;
  var tokenOperacion = req.params.token;

  if (esTokenErroneo(usuarioOperacion,tokenOperacion)){
    //console.log("Token usuario erroneo");
    var response = {
      "msg" : "Token usuario erroneo"
    }
    //res.status(404);
    res.send(response);
  }else{
    //console.log("Token usuario validado");
  var movil_new = req.body.movil;
  var email_new = req.body.email;
  var direccion_new = req.body.direccion;
  //console.log("cod_usuario es " + usuarioOperacion);
  //console.log("movil es " + movil_new);
  //console.log("email es " + email_new);
  //console.log("direccion es " + direccion_new);

  var queryUsuario = 'q={"cod_usuario" : "' + usuarioOperacion + '"}';
  //console.log("query es "+queryUsuario);

  var mLabCliente = requestJson.createClient(mLabURL);
  //console.log("Conectado con MLab");

  mLabCliente.get("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          //console.log("Error al buscar el usuario");
          response = {
              "msg" : "Error al buscar el usuario"
            }
            res.status(500);
            res.send(response);
        }else{
          if (body.length > 0){
            //console.log("Usuario encontrado");
            var putBody = '{"$set":{';
            putBody += '"movil" : "' + movil_new + '",';
            putBody += '"email" : "' + email_new + '",';
            putBody += '"direccion" : "' + direccion_new + '"';
            putBody += '}}';
            //console.log("PutBody is " + putBody);
            mLabCliente.put("appJCGUsuarios?" + queryUsuario + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT){
                  /*if (err){
                    console.log("Error al actualizar la clave de acceso");
                    var response = {
                        "msg" : "Error al actualizar la clave de acceso"
                    }
                    res.status(500);
                    res.send(response);
                  }else{*/
                    //console.log("UPDATE correcto");
                    var response = {
                      "msg" : "UPDATE correcto"
                    }
                    res.send(response);
                  //}
                }
              );
          }else{
              //console.log("El usuario no existe");
              var response = {
              "msg" : "El usuario no existe"
              }
              res.status(404);
              res.send(response);
          }
        }
      }
  );
}
}


module.exports.createUsuarioV1 = createUsuarioV1;
module.exports.reactivaClaveUsuarioV1 = reactivaClaveUsuarioV1;
module.exports.deleteUsuarioV1 = deleteUsuarioV1;
module.exports.getUsuarioByIdV1 = getUsuarioByIdV1;
module.exports.updateClaveUsuarioV1 = updateClaveUsuarioV1;
module.exports.updateDatosContactoV1 = updateDatosContactoV1;
