const requestJson = require('request-json');
const divisaAPIKey = "key=172|So^EmFyoZkYBfkXhEzZnU8N7GjQ4LWjN";

function listaDivisaV1(req,res){
  console.log("*******************************");
  console.log("GET /APIProyectoJCG/v1/divisas/");
  console.log("*******************************");

  var divisaURL = "http://api.currencies.zone";

  var divisaCliente = requestJson.createClient(divisaURL);
  //console.log("Conectado con API Cambio divisas");
  //console.log(divisaCliente);

  divisaCliente.get("/v1/full/EUR/json"+"?" + divisaAPIKey,
    function(err, resDivisa){
        if (err){
          //console.log("Error realizando consulta listado");
          //console.log(err);
          response = {
              "msg" : "Error realizando consulta listado"
            }
            res.status(500);
        }else{
          //console.log(resDivisa.body);
          if (resDivisa){
            //console.log("Listado de divisas");
            var response = resDivisa.body;
            //res.send(response);
          }else{
              //console.log("No hay divisas");
              var response = {
              "msg" : "No hay divisas"
              }
              res.status(404);
              //res.send(response);
          }
        }
        res.send(response);
      }
    );
}

function cambioDivisaV1(req,res){
  console.log("********************************");
  console.log("POST /APIProyectoJCG/v1/cambio/");
  console.log("********************************");

  var cantidadOrigen = req.body.quantity;
  var divisaOrigen = req.body.divisa_Origen;
  var divisaDestino = req.body.divisa_Destino;
  //console.log("cantidad es " + cantidadOrigen);
  //console.log("divisa Origen es " + divisaOrigen);
  //console.log("divisa Destino es " + divisaDestino);

  var divisaURL = "http://api.currencies.zone";

  var divisaCliente = requestJson.createClient(divisaURL);
  //console.log("Conectado con API Cambio divisas");
  //console.log(divisaCliente);

  divisaCliente.get("/v1/quotes/"+divisaOrigen+"/"+divisaDestino+"/json?quantity="+cantidadOrigen+"&" + divisaAPIKey,
    function(err, resDivisa){
        if (err){
          //console.log("Error realizando cambio divisa");
          //console.log(err);
          response = {
              "msg" : "Error realizando cambio divisa"
            }
            res.status(500);
        }else{
          //console.log(resDivisa.body);
          if (resDivisa){
            //console.log("Cambio divisa");
            var response = resDivisa.body;
            //res.send(response);
          }else{
              //console.log("No hay divisas");
              var response = {
              "msg" : "No hay divisas"
              }
              res.status(404);
              //res.send(response);
          }
        }
        res.send(response);
      }
    );
}

module.exports.listaDivisaV1 = listaDivisaV1;
module.exports.cambioDivisaV1 = cambioDivisaV1;

/*

****** ANTIGUO NOTIFICACION cuentasControlador *****

const AWSURL = "https://email.eu-west-1.amazonaws.com";

function cambioDivisaV1(req,res){
  console.log("*******************************************");
  console.log("GET /APIProyectoJCG/v1/envioNotificacionV1/");
  console.log("*******************************************");

  var awsCliente = requestJson.createClient(AWSURL);
  //awsCliente.headers['Content-Type'] = 'application/x-www-form-urlencoded';
  //awsCliente.headers['Host'] = 'email.eu-west-1.amazonaws.com';
  //awsCliente.headers['X-Amz-Date'] = '20180829T112010Z';
  //awsCliente.headers['Authorization'] = 'AWS4-HMAC-SHA256 Credential=AKIAJDSHTMIWFIMAA6RA/20180829/eu-west-1/ses/aws4_request, SignedHeaders=content-type;host;resource;x-amz-date, Signature=932d866f1187cff71b4953ed44659bea5ed431572e906405bb50b0a005bf4ec2';

  awsCliente.headers['Authorization'] = 'AWS AKIAJDSHTMIWFIMAA6RA:LPkBvrQ3Y5ixd9rRqLTdDZMzrQnyRjy0bWMMiDVb';
  //awsCliente.headers['Host'] = 'email.eu-west-1.amazonaws.com';
  //awsCliente.headers['X-Amz-Date'] = '20180829T110604Z';
  //awsCliente.headers['Authorization'] = 'AWS4-HMAC-SHA256 Credential=AKIAJDSHTMIWFIMAA6RA/20180829/eu-west-1/ses/aws4_request, SignedHeaders=content-type;host;resource;x-amz-date, Signature=21b402e695b3a92da32a5d582e168bbca7f1cfb8eff4efb8d1dc3088462418a5';

  console.log("Conectado con AWS");
  console.log(awsCliente);

  var parametros = '&Source=j.carmona%40bbva.com';
  parametros += '&Destination.ToAddresses.member.1=j.carmona%40bbva.com';
  parametros += '&Message.Subject.Data=This%20is%20the%20subject%20line.';
  parametros += '&Message.Body.Text.Data=Hello.%20I%20hope%20you%20are%20having%20a%20good%20day.';
  //parametros += '&AWSAccessKeyId=AKIAJDSHTMIWFIMAA6RA';
  //parametros += '&Signature=LPkBvrQ3Y5ixd9rRqLTdDZMzrQnyRjy0bWMMiDVb';
  //parametros += '&Algorithm=HMACSHA256';
  console.log("Parametros : "+parametros);

  awsCliente.get("?Action=SendEmail" + parametros,
    function(err, resAWS, body){
      if (err){
        console.log("Error realizando login : "+err);
      }else{
        console.log(body);
      }
    }
  );

}

module.exports.envioNotificacionV1 = envioNotificacionV1;
*/
