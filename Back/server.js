const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');

const port = process.env.PORT || 3000;

const bodyParser = require('body-parser');
app.use(bodyParser.json());

app.listen(port);
console.log("API escuchando en el puerto "+port);

const seguridadControlador     = require('./controladores/seguridadControlador');
const usuarioControlador     = require('./controladores/usuarioControlador');
const cuentasControlador     = require('./controladores/cuentasControlador');
const movimientosControlador = require('./controladores/movimientosControlador');
const divisaControlador = require('./controladores/divisaControlador');

/*** DIVISAS ***/
//Consulta de divisas
app.get("/APIProyectoJCG/v1/divisas", divisaControlador.listaDivisaV1);
//Cambio de divisas
app.post("/APIProyectoJCG/v1/cambio", divisaControlador.cambioDivisaV1);

/*** SEGURIDAD ***/
//Login de un usuario
app.post("/APIProyectoJCG/v1/login", seguridadControlador.loginUsuarioV1);
//Logout de un usuario
app.post("/APIProyectoJCG/v1/logout", seguridadControlador.logoutUsuarioV1);

/*** CUENTAS ***/
//Crear una cuenta de un usuario en appJCGCuentas
app.post('/APIProyectoJCG/v1/appJCGCuentas/:idUsuario/:token', cuentasControlador.createCuentaUsuarioV1);
//Listado de cuentas de un usuario de appJCGCuentas
app.get('/APIProyectoJCG/v1/appJCGCuentas/:idUsuario/:token', cuentasControlador.getCuentasUsuarioV1);
//Dar de baja una cuenta de un usuario en appJCGCuentas
app.put('/APIProyectoJCG/v1/appJCGCuentas/:idUsuario/:idCuenta/:token', cuentasControlador.deleteCuentaUsuarioV1);

/*** USUARIOS ***/
//Crear un usuario en appJCGUsuarios
app.post('/APIProyectoJCG/v1/appJCGUsuarios',usuarioControlador.createUsuarioV1);
//Reactivar la clave de acceso de un usuario en appJCGUsuarios
app.put('/APIProyectoJCG/v1/appJCGUsuarios/reactivaClaveUsuario',usuarioControlador.reactivaClaveUsuarioV1);
//Dar de baja un usuario en appJCGUsuarios
app.put('/APIProyectoJCG/v1/appJCGUsuarios/bajaUsuario', usuarioControlador.deleteUsuarioV1);
//Consultar un usuario de appJCGUsuarios
app.get('/APIProyectoJCG/v1/appJCGUsuarios/:idUsuario/:token',usuarioControlador.getUsuarioByIdV1);
//Actualizar la clave de acceso de un usuario en appJCGUsuarios
app.put('/APIProyectoJCG/v1/appJCGUsuarios/updateClaveUsuario/:token',usuarioControlador.updateClaveUsuarioV1);
//Actualizar los datos de contacto de un usuario en appJCGUsuarios
app.put('/APIProyectoJCG/v1/appJCGUsuarios/datosContacto/:token',usuarioControlador.updateDatosContactoV1);

/*** MOVIMENTOS ***/
//Crear un movimiento (INGRESO) en una cuenta de appJCGMovimientos
app.post('/APIProyectoJCG/v1/appJCGMovimientos/operacionCajero/:token', movimientosControlador.createOperacionCajeroV1);
//Crear un movimiento (TRANSFERENCIA/TRASPASO) en una cuenta de appJCGMovimientos
app.post('/APIProyectoJCG/v1/appJCGMovimientos/envioDinero/:token', movimientosControlador.createEnvioDineroV1);
//Listado de movimientos de una cuenta de un usuario de appJCGMovimientos
app.get('/APIProyectoJCG/v1/appJCGMovimientos/:idUsuario/:idCuenta/:token', movimientosControlador.getMovimientosCuentaUsuarioV1);
